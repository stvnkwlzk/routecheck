class Cell():
    def __init__(self, realx, realy, lat, lng, hbounds, vbounds):
        self.realx = realx
        self.realy = realy
        self.lat = lat
        self.lng = lng
        self.hbounds = hbounds
        self.vbounds = vbounds
        self.inBounds = False

    def hasInBounds(self, lat, lng):
        #print("CHECK:")
        #print(str(lat) + ": " + str(lng))
        #print("AGAINST BETWEEN")
        #print("Lat:" + str(self.lat-self.vbounds) + " - " + str(self.lat))
        #print("Lng:" + str(self.lng-self.hbounds) + " - " + str(self.lng))
        if self.inBounds:
            return True
        if lat >= (self.lat-self.vbounds) and lat <= (self.lat) and lng >= (self.lng-self.hbounds) and lng <= (self.lng):
            self.inBounds = True
            return True
        else:
            return False
