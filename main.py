from pprint import pprint
from cell import Cell
from grid import Grid
import json

def main():
    polyline1 = returnPolyLine('FF8B4174-2129-43CF-A5E8-7A43E49C2882.json', 0)
    polyline2 = returnPolyLine('FF8B4174-2129-43CF-A5E8-7A43E49C2882.json', 1)
    gridpoints1 = findRegionSpan(polyline1)
    gridpoints2 = findRegionSpan(polyline2)
    grid1 = Grid(gridpoints1["northlat"], gridpoints1["southlat"], gridpoints1["westlng"], gridpoints1["eastlng"], 15, 15)
    #print(gridpoints)
    grid1.checkPolyline(polyline1)
    grid2 = Grid(gridpoints1["northlat"], gridpoints1["southlat"], gridpoints1["westlng"], gridpoints1["eastlng"], 15, 15)
    grid2.checkPolyline(polyline2)
    compareGrids(grid1, grid2)

def findRegionSpan(polyline):
    print("test")
    gridpoints = {"northlat": polyline[0]["lat"],
                  "westlng": polyline[0]["lng"],
                  "southlat": polyline[0]["lat"],
                  "eastlng": polyline[0]["lng"]}
    for index, point in enumerate(polyline):
        if point["lat"] > gridpoints["northlat"]:
            gridpoints["northlat"] = point["lat"]
        if point["lat"] < gridpoints["southlat"]:
            gridpoints["southlat"] = point["lat"]
        if point["lng"] < gridpoints["westlng"]:
            gridpoints["westlng"] = point["lng"]
        if point["lng"] > gridpoints["eastlng"]:
            gridpoints["eastlng"] = point["lng"]
    return gridpoints

def returnPolyLine(filename, route):
    with open(filename) as file:
        data = json.load(file)
    return data[0]["routes"][route]["polyline"]

def compareGrids(grid1, grid2):
    errorCount = 0.0
    for i in range(0, len(grid1.grid)):
        for j in range(0, len(grid1.grid[i])):
            if grid1.checkedGrid[i][j] == grid2.checkedGrid[i][j]:
                #print("matching")
                pass
            else:
                errorCount = errorCount + 1
    # DRAW GRID WITH MATCHING AND NOT MATCHING INBOUND CELLS
    cellCount = grid1.getCellCount()
    print("There are " + str(cellCount) + " cells.")#
    errorPercentage = (errorCount/cellCount)*100
    print("There are " + str(errorCount) + " errors, which is " + str(errorPercentage) + "%")

main()
