from pprint import pprint
from cell import Cell
from numpy import matrix

class Grid():
    def __init__(self, north, south, west, east, hsteps, vsteps):
        self.north = north
        self.south = south
        self.west = west
        self.east = east
        self.hsteps = hsteps
        self.vsteps = vsteps
        self.basePolyline = False
        self.calculateDiff()
        self.calcStepSize()
        self.makeCells()

        print("Grid initializing....")

    def calculateDiff(self):
        if (self.west < 0 and self.east > 0) or (self.west > 0 and self.east < 0):
            raise ValueError("This does not work yet with west and eas values crossing the 0degree line")
        if (self.west < 0 and self.east > 0) or (self.west > 0 and self.east < 0):
            raise ValueError("This does not work yet with south and north values crossing the equator")
        self.latDiff = self.north - self.south
        self.lngDiff = self.east - self.west
        if self.latDiff < 0:
            self.latDiff = self.latDiff * -1
        if self.lngDiff < 0:
            self.lngDiff = self.lngDiff * -1
        print(self.lngDiff)
        print(self.latDiff)

    def getCellCount(self):
        return len(self.grid) * len(self.grid[0])

    def calcStepSize(self):
        self.hstepSize = self.lngDiff / self.hsteps
        self.vstepSize = self.latDiff / self.vsteps
        print(self.hstepSize)
        print(self.vstepSize)

    def makeCells(self):
        self.grid = []
        for i in range(0, self.hsteps):
            self.grid.append([])
            for j in range(0, self.vsteps):
                self.grid[i].append(
                    Cell(i, j, self.vstepSize*(i+1), self.hstepSize*(j+1), self.hstepSize, self.vstepSize)
                )
                #self.grid[i].append({"lng": self.hstepSize*(i+1), "lat": self.vstepSize*(j+1)})
        pprint(self.grid)

    def checkPolyline(self, locations):
        #self.checkedGrid = [][]
        #inCellString = "x"

        self.checkedGrid = [[False] * len(self.grid[0]) for i in range(0, len(self.grid))]
        for loc in locations:
            for i in range(0, len(self.grid)):
                for j in range(0, len(self.grid[i])):
                    if self.grid[i][j].hasInBounds(self.north - loc["lat"], loc["lng"] - self.west):
                        self.checkedGrid[i][j] = "x"
                    else:
                        self.checkedGrid[i][j] = "o"
        #pprint(self.checkedGrid)
        print(matrix(self.checkedGrid))
